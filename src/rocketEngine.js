const fuelConsumption = 4; // kg/s
const rocketMass = 120; // кг
const gasVelocity = 1000; // м/с
const g = 9.83; // м/c^2

export function calcAcceleration(time, position) {
  // console.log(rocketMass - time * fuelConsumption)
  const mass = rocketMass - time * fuelConsumption(position);
  const acceleration = (fuelConsumption(position) * gasVelocity / mass) - g(position);
  return {mass, acceleration};
}

export function calcPosition(prevVelocity, prevPosition, acceleration, deltaTime) {
  const velocity = prevVelocity + acceleration*deltaTime;
  const position = prevPosition + velocity*deltaTime;
  return {velocity, position};
}