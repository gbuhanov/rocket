import React, { Component } from 'react';
import autobind from 'autobind-decorator';
import logo from './logo.svg';
import './App.css';
import {calcAcceleration, calcPosition} from './rocketEngine';

class App extends Component {
  state = {
    time: 0,
    acc: 0,
    now: 0,
    velocity: 0,
    position: 0
  }

  update() {
    if (this.state.pause) return;
    const currentTime = new Date().getTime();
    const deltaTime = this.state.now ? currentTime - this.state.now : 0;
    const time = this.state.time + deltaTime;
    const {acceleration, mass} = calcAcceleration(time / 1000);
    const {velocity, position} = calcPosition(this.state.velocity, this.state.position, acceleration, deltaTime / 1000);

    this.setState({
      time,
      now: currentTime,
      acc: acceleration,
      mass,
      velocity,
      position
    });

    requestAnimationFrame(timestamp => {this.update(timestamp)});
  }

  handleStart() {
    this.setState({
      pause: false
    })
    requestAnimationFrame(timestamp => {this.update(timestamp)});
  }

  handlePause() {
    this.setState({
      pause: true
    })
  }

  handleReset() {
    this.setState({
      pause: false,
      time: 0,
      now: 0,
      acc: 0,
      mass: 0,
      velocity: 0,
      position: 0
    })
  }

  render() {
    const {time, mass, velocity, position, acc} = this.state;

    return (
      <div className="App">
        <div>Time: {time / 1000} (s)</div>
        <div>Acceleration: {acc} (m / s^2)</div>
        <div>Mass: {mass} (kg)</div>
        <div>Velocity: {velocity} (m / s)</div>
        <div>Position: {position} (m)</div>
        <button onClick={this.handleStart}>Start</button>
        <button onClick={this.handlePause}>Stop</button>
        <button onClick={this.handleReset}>Reset</button>
      </div>
    );
  }
}

export default autobind(App);
